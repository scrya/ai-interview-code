# Python model for ai-interview-code
`StateModel(input_shape, num_output_nodes, hidden_layers=1, search_method='grid', loss_func='categorical_crossentropy', metrics=['accuracy'], optimizer='rmsprop')`

A python model that creates a fully connected neural network using Keras.
The model has the following parameters:

| Parameter        | type           | Description  |
| ------------- |:-------------| :-----|
| `input_shape`      | tuple | The shape of the input being passed into the input layer |
| `num_output_nodes`      | int      |   The number of nodes expected in the output layer |
| `search_method` | {'grid', 'manual'} | The method used to create the hidden layers in the neural network. If set to `grid`, creates hidden layers with sizes of powers of two. The size of the last hidden layer is the smallest power of two greater than `num_output_nodes`. If set to `manual`, allows the user to define the number and size of the hidden layers. The default value is `grid`.
| `hidden_layers` | int / array      | Defines the shape of the hidden layers according to `search_method`. If `search_method` is set to `grid`, accepts an integer which defines the number of hidden layers. If `search_method` is set to `manual`, takes an array where each value is the size of a hidden layer. For example, a value of '3' when using `grid` and a `num_output_nodes` of '3' is equivalent to a value of '[16, 8, 4]' when using `manual`. The default value is '1'. |
| `loss_func` | string | Takes the name of a loss function to apply to the model. Default value is `categorical_crossentropy` Accepted values are: `mean_squared_error`, `mean_absolute_error`, `mean_absolute_percentage_error`, `mean_squared_logarithmic_error`, `squared_hinge`, `hinge`, `categorical_hinge`, `categorical_crossentropy`, `sparse_categorical_crossentropy`, `binary_crossentropy`, `kullback_leibler_divergence`, `poisson`, and `cosine_proximity`. |
| `metrics` | array | The metric used to determine the performance of the model. Default is `['accuracy']`. |
| `optimizer` | string | The optimizer used in the model. The default value is `rmsprop`. Accepted values are: `rmsprop`, `adam` and `sgd`. |

| Method | |
| -------|-|
| `fit (X, Y[, batch_size=32, epochs=10])` | Fits the model to a matrix X and labels Y. Set `batch_size` change the number of samples per gradiant update. Set `epochs` to determine the number of epochs to train the model to. | 
| `score(X, Y)` | Returns the loss and accuracy values for the X and Y values when evaluated using the model. |
