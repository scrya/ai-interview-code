from keras.models import Sequential
from keras.layers import Dense, Activation
from keras import optimizers

class StateModel:
    def __init__(self, 
                input_shape, 
                num_output_nodes, 
                hidden_layers=1, 
                search_method='grid',
                loss_func='categorical_crossentropy',
                metrics=['accuracy'],
                optimizer='rmsprop'):

        if search_method != 'grid' and search_method != 'manual':
            raise ValueError('Value ' + str(search_method) + ' is of incorrect form for parameter search_method')

        if search_method == 'grid' and (not isinstance(hidden_layers, int) or hidden_layers < 1):
            raise ValueError('Invalid number of layers for search method grid')

        if search_method == 'manual' and (not isinstance(hidden_layers, list)):
            raise ValueError('Invalid format for layers for search method manual')

        LOSS_FUNCS = ['mean_squared_error', 
                      'mean_absolute_error', 
                      'mean_absolute_percentage_error', 
                      'mean_squared_logarithmic_error', 
                      'squared_hinge', 
                      'hinge', 
                      'categorical_hinge', 
                      'categorical_crossentropy', 
                      'sparse_categorical_crossentropy', 
                      'binary_crossentropy', 
                      'kullback_leibler_divergence', 
                      'poisson', 
                      'cosine_proximity']

        OPTIMIZERS = ['rmsprop', 'sgd', 'adam']

        if loss_func not in LOSS_FUNCS:
            raise ValueError('Loss function ' + loss_func + ' is not valid')

        if optimizer not in OPTIMIZERS:
            raise ValueError('Optimizer ' + optimizer + ' is not valid')

        self.model = Sequential()
        self.__addLayers(input_shape, num_output_nodes, hidden_layers, search_method)
        self.__compile(loss_func, metrics)

    def __firstLayerSize(self, outputNodes):
        return 1 if outputNodes == 0 else 2**(outputNodes - 1).bit_length()

    def __addLayers(self, input_shape, output_nodes, layers, searchMethod):
        if searchMethod == 'grid':
            self.__addLayersGrid(input_shape, output_nodes, layers)
        else:
            self.__addLayersManual(input_shape, output_nodes, layers)

    def __addLayersGrid(self, input_shape, output_nodes, num_layers):
        if num_layers > 1:
            second_layer_size = self.__firstLayerSize(num_layers)
            layer_size = second_layer_size * 2**(num_layers - 2)

            self.model.add(Dense(layer_size, input_shape=input_shape))
            self.model.add(Activation('relu'))

            for i in range(num_layers - 1, 0, -1):
                layer_size /= 2
                layer_size = int(layer_size)
                self.model.add(Dense(layer_size))
                self.model.add(Activation('relu'))
            self.model.add(Dense(output_nodes))
            self.model.add(Activation('softmax'))
        else:
            self.model.add(Dense(output_nodes, input_shape=input_shape))
            self.model.add(Activation('softmax'))

    def __addLayersManual(self, input_shape, output_nodes, layers):
        if len(layers) < 1:
            self.model.add(Dense(output_nodes, input_shape=input_shape))
            self.model.add(Activation('softmax'))
        else:
            for layer_size in layers:
                self.model.add(Dense(layer_size))
                self.model.add(Activation('relu'))
            self.model.add(Dense(output_nodes))
            self.model.add(Activation('softmax'))

    def __compile(self, loss_func, metrics):
        opt = optimizers.RMSprop()
        self.model.compile(opt, loss=loss_func, metrics=metrics)

    def fit(self, x, y, batch_size=32, epochs=10):
        self.batch_size = batch_size
        self.model.fit(x, y, epochs=epochs, batch_size=self.batch_size)

    def score(self, x, y):
        score = self.model.evaluate(x, y, batch_size=self.batch_size)
        return score
